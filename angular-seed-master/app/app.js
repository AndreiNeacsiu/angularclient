'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.userControl',
  'myApp.authentication',
  'myApp.userData',
  'myApp.view1',
  'myApp.view2',
  'ngCookies',
  'ngStorage',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', '$httpProvider', function($locationProvider, $routeProvider, $httpProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.otherwise({redirectTo: '/authentication'});
  $httpProvider.interceptors.push('tokenMarker');
}]);
