'use strict';

angular.module('myApp.authentication', ['ngRoute'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/authentication', {
    templateUrl: 'authentication/authentication.html',
    controller: 'AuthenticationCtrl'
  });
}])
.service('shareError', function (){
  var errorMsg = null;
  return {
    getError: function () {
      return errorMsg;
    },
    setError: function(value) {
      errorMsg = value;
    }
  };
})
.factory('tokenMarker', ['$q', '$localStorage', '$cookies' , '$window', '$location', 'shareError', function($q, $localStorage, $cookies, $window, $location, shareError) {  
    var tokenMarker = {
        request: function(config) {
            if($localStorage['tokenStorage'] != null)
            {
                config.headers['Authorization'] = 'Bearer ' + $localStorage['tokenStorage'];
            } 
            else if($cookies.get('tokenCoockies') != null){
                config.headers['Authorization'] = 'Bearer ' + $cookies.get('tokenCoockies');
            } 
            return config;
        },
        responseError: function (response)  
        {  
            if(response.status != 200){
                shareError.setError('You have been discounted, please log in');
                $location.path('/authentication');
            }
            return $q.reject(response);  
        }  
    };
    return tokenMarker;
}])
.controller('AuthenticationCtrl',['$scope', '$http', '$cookies', '$localStorage', '$window', '$location', 'shareError',function($scope, $http, $cookies, $localStorage, $window, $location, shareError)  {
 if(shareError.getError()!=null){
     $scope.error = shareError.getError();
 }
  $scope.submitForm = function() {
  $http({
    method  : 'POST',
    url     : 'http://localhost:8091/ProjectV2/webapi/myresource/authentication',
    data    :  angular.toJson($scope.user), 
    headers :  { 'Content-Type' : 'application/json'  }  
    })
      .then(function successCallback(response) {
          if(response){
            $localStorage['tokenStorage'] = response.data;
            $cookies.put('tokenCoockies',response.data); 
            $location.path('/userControl');
          }
      }, function errorCallback(response) {
            $scope.error = 'Username or password is incorrect';
      });
  }
}]);
