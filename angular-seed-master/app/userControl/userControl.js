'use strict';

angular.module('myApp.userControl', ['ngRoute'])
.config(['$sceDelegateProvider', function($sceDelegateProvider) {
  // We must whitelist the JSONP endpoint that we are using to show that we trust it
  $sceDelegateProvider.resourceUrlWhitelist([
    'self',
    'https://angularjs.org/**'
  ]);
}])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/userControl', {
    templateUrl: 'userControl/userControl.html',
    controller: 'userControl',
  })
}])
.service('shareUser', function (){
  var userShare = {
    id:-1,
    name:"",
    password:""
  };
  return {
    getUser: function () {
      return userShare;
    },
    setUser: function(value) {
      userShare = value;
    } ,   
    setID: function(value) {
      userShare.id = -1;
    },
    resetUser:function(){
      userShare = {
      id:-1,
      name:"",
      password:""
      };
    }
  };
})
.controller('userControl',['$scope', '$http','$location','shareUser', '$cookies', '$localStorage', 'shareError',function($scope, $http, $location, shareUser ,$cookies, $localStorage, shareError) {
 // getAllUsers();
 if($localStorage['tokenStorage'] != null && $cookies.get('tokenCoockies') != null)
 {
    getUserPage(0);
    $scope.currentPage = 0;
    shareUser.resetUser();
 }
 else{
   shareError.setError('UNAUTHORIZED');
   $location.path('/authentication');
 }
  $scope.logOut = function()
  {
    shareError.setError(null);
    delete $localStorage['tokenStorage'];
    $cookies.remove('tokenCoockies');
    $location.path('/authentication');
  } 
  function getUserPage(index){
     $http({
      method: 'GET',
      url: 'http://localhost:8091/ProjectV2/webapi/myresource/usersPage/' + index,
      dataType: 'jsonp'
      }).
        then(function successCallback(response) {
          $scope.users = response.data;
          getUserNumbers();
        });
  }
  function getAllUsers(){
    $http({
      method: 'GET',
      url: 'http://localhost:8091/ProjectV2/webapi/myresource/get',
      dataType: 'jsonp'
      }).
        then(function successCallback(response) {
          $scope.users = response.data;
          getUserNumbers();
        });
  }
  function getUserNumbers(){
     $http({
      method: 'GET',
      url: 'http://localhost:8091/ProjectV2/webapi/myresource/usersNumber',
      }).
        then(function successCallback(response) {
          //console.log(response.data);
          $scope.numberUsers = response.data;
          countPages();
        });   
  }
  function countPages(){ 
    var copynumberUsers = $scope.numberUsers;
    if(copynumberUsers%10 == 0)
        $scope.numberPages = 0;
    else 
      $scope.numberPages = 1; 
    while(copynumberUsers>0){
      if(copynumberUsers%10 == 0)
        $scope.numberPages++;
      copynumberUsers--;
    }
  }
  $scope.deleteUser = function(user){
    $http({
    method: 'DELETE',
    url: 'http://localhost:8091/ProjectV2/webapi/myresource/remove/' + user.id
      }).then(success,error);
  }
  $scope.getNumber = function(num) {
    return new Array(num);   
  }
  $scope.getUserPage = function(index)
  {
    $scope.currentPage = index;
    getUserPage(index);
  }
  $scope.editUser = function(user){
    shareUser.setUser(user);
     $location.path('/userData');
  }
  $scope.redirect = function()
  {
      $location.path('/userData');
     
  } 
  function success(response) {  
    if(($scope.numberUsers - 1) == $scope.currentPage*10)
     {
       $scope.currentPage--;
      getUserPage($scope.currentPage); 
     }
    else
    {
      getUserPage($scope.currentPage);  
    }
  }
  function error(response) {   
  }
}]);