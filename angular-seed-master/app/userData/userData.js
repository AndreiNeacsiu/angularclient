'use strict';

angular.module('myApp.userData', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/userData', {
    templateUrl: 'userData/userData.html',
    controller: 'userData'
  });
}])
.controller('userData', ['$scope', '$http','$location','shareUser', 'shareError', '$cookies', '$localStorage' ,function($scope, $http, $location,shareUser, shareError, $cookies, $localStorage) {

  $scope.user = {
    id:-1,
    name:"",
    password:""
  };
  //console.log(sharedName.getProperty());
if($localStorage['tokenStorage'] != null && $cookies.get('tokenCoockies') != null)
 {
  if(shareUser.getUser().id == -1)
    $scope.value = "Create a new user"; 
  else  
     $scope.value = "Welcome " + shareUser.getUser().name+ " please insert new data"; 
  $scope.submitForm = function() {
  if(shareUser.getUser().id == -1)
  {
  $http({
    method  : 'POST',
    url     : 'http://localhost:8091/ProjectV2/webapi/myresource/add',
    data    :  angular.toJson($scope.user), 
    headers :  { 'Content-Type' : 'application/json'  }  
    })
      .then(function successCallback(response) {
          console.log(response.data);  
          $location.url('/userControl').replace();
      }, function errorCallback(response) {
        console.log(response.data);
      });
  }
   else{
   $scope.user.id = shareUser.getUser().id;
   $http({
   method  : 'PUT',
   url     : 'http://localhost:8091/ProjectV2/webapi/myresource/update',
   data    :  angular.toJson($scope.user), 
   headers :  { 'Content-Type' : 'application/json'  }  
      })
      .then(function successCallback(response) {
         console.log(response.data);  
        // $location.url('/userControl').replace();
        $location.path('/userControl');
      }, function errorCallback(response) {
        console.log(response.data);
      });
   }    
  };    
 } 
else{
   shareError.setError('UNAUTHORIZED');
   $location.path('/authentication');
 }
  $scope.logOut = function()
  {
    shareError.setError(null);
    delete $localStorage['tokenStorage'];
    $cookies.remove('tokenCoockies');
    $location.path('/authentication');
  } 
  $scope.redirectToTest = function()
  {
    $location.path('/userControl');
  }   
    function success(response) {  
      console.log(response.data);  
      redirectToTest();  
  }
  function error(response) {  
    console.log(response.data);  
  } 
}]);

